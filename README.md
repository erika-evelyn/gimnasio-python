# Gimnasio Python

Deberás crear un algoritmo para un gimnasio donde el usuario pueda cargar:
- [ ] Clientes: nombre, apellido, dni, edad, dirección, si tiene apto médico.
- [ ] Clases: nombre de la clase, horario, abono por clase.
- [ ] Facturación: se debe poder elegir un cliente, una clase, ingresar la cantidad de clases tomadas y ver el total.
- [ ] Reportes: debe mostrar resultados gráficos a tu criterio. Ej: crecimiento de clientes, o ingresos diarios, etc.

