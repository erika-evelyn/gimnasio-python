import sys

import pymysql
import matplotlib.pyplot as plt
import pandas

# Abre conexion con la base de datos
db = pymysql.connect(host='sql10.freemysqlhosting.net',
                             user='sql10388867',
                             password='8gHpy1gZiS',
                             database='sql10388867')


# Armo menu
def VerMenu():
  print("****MENÚ PRINCIPAL****. ")
  print("Elija una opción para continuar: ")
  print("1. Dar de alta un cliente. ")
  print("2. Dar de alta una clase. ")
  print("3. Facturar. ")
  print("4. Ver reporte. ")
  print("5. Salir. ")
  opcion = int(input())
  return opcion

def SeleccionarOpcionMenu():
  opcion = VerMenu()
  if opcion == 1:
    AgregarCliente()
  elif opcion == 2:
    AgregarClase()
  elif opcion == 3:
    AgregarCantidadClasesACliente()
  elif opcion == 4:
    VerReportes()
  elif opcion == 5:
    sys.exit("Sesión cerrada. ¡Gracias por elegirnos! ")
  else:
    print("La opción elegida es inválida. ")
    SeleccionarOpcionMenu()
  SeleccionarOpcionMenu()

def AgregarCliente():
  print("Ingrese el nombre del cliente: ")
  nombre = input()
  print("Ingrese el apellido del cliente: ")
  apellido = input()
  print("Ingrese el DNI del cliente: ")
  dni = input()
  print("Ingrese la dirección del cliente: ")
  direccion = input()
  print("Ingrese la edad del cliente: ")
  edad = input()
  print("Tiene apto médico? Ingrese 1 si tiene o cualquier otro número si no tiene.")
  aptoMedico = input()
  if(aptoMedico == 1):
    tieneAptoMedico = True;
  else:
    tieneAptoMedico = False;
  GuardarCliente(nombre, apellido, dni, direccion, edad, tieneAptoMedico)
  SeleccionarOpcionMenu()


def AgregarClase():
  print("Ingrese el nombre de la clase: ")
  nombre = input()
  print("Ingrese el horario de la clase: ")
  horario = input()
  print("Ingrese el abono de la clase: ")
  abono = input()
  GuardarClase(nombre, horario, abono)
  SeleccionarOpcionMenu()

def AgregarCantidadClasesACliente():
  print("Ingresa la opcion que corresponda al cliente a facturar:")
  VerClientes()
  idCliente = int(input())
  print("Ingresa la opcion que corresponda a la clase a facturar:")
  VerClases()
  idClase = int(input())
  print("Ingresa la cantidad clases tomadas por el cliente:")
  cantidadClases = int(input())
  total = cantidadClases * float(GetAbonoByClaseId(idClase))
  Facturar(idCliente, idClase, cantidadClases, total)
  SeleccionarOpcionMenu()


def VerReportes():
    VerReporteFacturacionPorCliente()
    VerReporteFacturacionPorClase()

def VerReporteFacturacionPorCliente():
    # Prepare a cursor object using cursor() method
    cursor = db.cursor()

    try:
        # Prepare SQL query to INSERT a record into the database.
        sql = "SELECT c.Nombre, c.Apellido, f.Total FROM Facturacion f inner join Clientes c " \
              "on f.IdCliente = c.Id"

        # Execute the SQL command
        cursor.execute(sql)

        # Fetch all the rows in a list of lists.
        results = cursor.fetchall()

        if(any(results)):
            print("Totales facturados por cliente.")
            data = pandas.read_sql(sql, db, index_col=['Nombre'], columns=['Nombre', 'Apellido'])
            print(data)
            plt.plot(data.Total)
            plt.suptitle('Totales facturados por cliente')
            plt.show()
        else:
            print("No hay datos para generar el reporte.")
            AgregarClase()
    except:
        print("Ocurrió un error al generar reporte.")
        SeleccionarOpcionMenu()

    # disconnect from server
    cursor.close()

def VerReporteFacturacionPorClase():
    # Prepare a cursor object using cursor() method
    cursor = db.cursor()

    try:
        # Prepare SQL query to INSERT a record into the database.
        sql = "SELECT c.Nombre, f.Total FROM Facturacion f inner join Clases c " \
              "on f.IdClase = c.Id"

        # Execute the SQL command
        cursor.execute(sql)

        # Fetch all the rows in a list of lists.
        results = cursor.fetchall()

        if(any(results)):
            print("Totales facturados por clase.")
            data = pandas.read_sql(sql, db, index_col=['Nombre'])
            dataGroup = data.groupby(['Nombre']).sum()
            print(dataGroup)
            plt.suptitle('Totales facturados por clase')
            plt.plot(dataGroup)
            plt.show()
        else:
            print("No hay datos para generar el reporte.")
            AgregarClase()
    except:
        print("Ocurrió un error al generar reporte.")
        SeleccionarOpcionMenu()

    # disconnect from server
    cursor.close()


def GuardarClase(nombre, horario, abono):
    # Prepare a cursor object using cursor() method
    cursor = db.cursor()
    # Prepare SQL query to INSERT a record into the database.
    sql = "INSERT INTO Clases(Nombre, Horario, Abono) \
       VALUES ('{0}','{1}', '{2}')".format(nombre, horario, abono)
    try:
       # Execute the SQL command
       cursor.execute(sql)
       # Commit your changes in the database
       db.commit()
       print("Guardado con éxito.")
    except:
       # Rollback in case there is any error
       db.rollback()
       print("Ocurrió un error al guardar.")
       SeleccionarOpcionMenu()


    # desconectar del servidor
    cursor.close()


def GuardarCliente(nombre, apellido, dni, direccion, edad, tieneAptoMedico):
    # Prepare a cursor object using cursor() method
    cursor = db.cursor()

    # Prepare SQL query to INSERT a record into the database.
    sql = "INSERT INTO Clientes(Nombre, Apellido, DNI, Direccion, Edad, AptoMedico) \
       VALUES ('{0}','{1}', '{2}', '{3}', '{4}', '{5}')".format(nombre, apellido, dni, direccion, int(edad), tieneAptoMedico)
    try:
        # Execute the SQL command
        cursor.execute(sql)
        # Commit your changes in the database
        db.commit()
        print("Guardado con éxito.")
    except:
        # Rollback in case there is any error
        db.rollback()
        print("Ocurrió un error al guardar.")
        SeleccionarOpcionMenu()

    # desconectar del servidor
    cursor.close()

def VerClases():
    # Prepare a cursor object using cursor() method
    cursor = db.cursor()

    try:
        # Prepare SQL query to INSERT a record into the database.
        sql = "SELECT * FROM Clases"
        # Execute the SQL command
        cursor.execute(sql)

        # Fetch all the rows in a list of lists.
        results = cursor.fetchall()

        if(any(results)):
            for row in results:
                Id = row[0]
                Nombre = row[1]
                Horario = row[2]
                Abono = row[3]
                print("{0} - {1} {2} - ${3}.-".format(Id, Nombre, Horario, Abono))
        else:
            print("Debe tener agendada al menos una Clase para poder facturar.")
            AgregarClase()
    except:
        print("Ocurrió un error al Consultar los clientes.")
        SeleccionarOpcionMenu()

    # disconnect from server
    cursor.close()

def VerClientes():
    # Prepare a cursor object using cursor() method
    cursor = db.cursor()

    try:
        # Prepare SQL query to INSERT a record into the database.
        sql = "SELECT * FROM Clientes"
        # Execute the SQL command
        cursor.execute(sql)

        # Fetch all the rows in a list of lists.
        results = cursor.fetchall()

        if (any(results)):
            for row in results:
                Id = row[0]
                Nombre = row[1]
                Apellido = row[2]
                DNI = row[3]
                Edad = row[4]
                Direccion = row[5]
                AptoMedico = row[6]
                print("{0} - {1} {2} - DNI: {3}".format(Id, Nombre, Apellido, DNI))
        else:
            print("Debe tener agendada al menos un cliente para poder facturar.")
            AgregarCliente()

    except:
       print("Ocurrió un error al Consultar los clientes.")
       SeleccionarOpcionMenu()
    # disconnect from server
    cursor.close()


def GetAbonoByClaseId(claseId):
    # Prepare a cursor object using cursor() method
    cursor = db.cursor()

    try:
        # Prepare SQL query to INSERT a record into the database.
        sql = "SELECT * FROM Clases where Id = {0}".format(claseId)
        # Execute the SQL command
        cursor.execute(sql)

        # Fetch all the rows in a list of lists.
        results = cursor.fetchall()

        if(any(results)):
            for row in results:
                Id = row[0]
                Nombre = row[1]
                Horario = row[2]
                Abono = row[3]
                return Abono

    except:
       print("Ocurrió un error al Consultar el abono de clase.")
       SeleccionarOpcionMenu()
    # disconnect from server
    cursor.close()

def Facturar(idCliente, idClase, cantidadClases, total):
    # Prepare a cursor object using cursor() method
    cursor = db.cursor()

    # Prepare SQL query to INSERT a record into the database.
    sql = "INSERT INTO Facturacion(IdCliente, IdClase, CantidadClases, Total) \
       VALUES ('{0}','{1}', '{2}', '{3}')".format(idCliente, idClase, cantidadClases, total)
    try:
        # Execute the SQL command
        cursor.execute(sql)
        # Commit your changes in the database
        db.commit()
        print("Facturado con éxito.")
    except:
        # Rollback in case there is any error
        db.rollback()
        print("Ocurrió un error al guardar.")
        SeleccionarOpcionMenu()

    # desconectar del servidor
    cursor.close()


SeleccionarOpcionMenu()